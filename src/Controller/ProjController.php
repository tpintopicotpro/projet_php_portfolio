<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Projets;
use App\Form\AddProjetType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

class ProjController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $projet = new Projets();
        // ...

        $form = $this->createForm(AddProjetType::class, $projet,[
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($projet);
            $entityManager->flush();
        }
        return $this->renderForm('proj/index.html.twig', [
            'form' => $form,
        ]);
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Projets;
use Symfony\Component\Form\FormBuilderInterface;

class ProjetsController extends AbstractController
{
    #[Route('/projets', name: 'projets')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Projets::class)->findAll();
        $projets = $repository;
        return $this->render('projets/index.html.twig', [
            'controller_name' => 'ProjetsController',
            'projets' => $projets
        ]);
    }
   
}
